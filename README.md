<p align="center">
  <img height="150" src=doc/ressources/3IT_VH-C-400px.png>
  <img height="150" src=doc/ressources/LN2_logo_couleur_New_300dpi.jpg>

# Panel-Orientation

## Table of content

1. [Introduction](#introduction)
2. [Prerequisites](#Prerequisites)
3. [Demo mode](#demo-mode)
4. [Help command](#help-command)
5. [Parameters](#parameters)
7. [Code structure](#code-structure)

## Introduction

This project contains a Python package to find optimal tilt values and scheduling dates for a PV panel.
By following the steps presented below, you will be able to find
optimal tilt values and scheduling dates for your own installation.
You can specify the PVLib model, the location and the meteorological dataset.
An example is provided for a generic PV panel located in Sherbrooke with PVGIS TMY data,
but you can directly add a new location and then start a simulation with your own parameters.

## Prerequisites

* Install Python 3.10+

* Install the required project dependencies:

```
pip3 install -r requirements.txt
```

## Help command

You can find the documentation of the command line interface in the [`panel_orientation`](panel_orientation) package
with the following command lines:

```
cd panel_orientation

python -m cli -h
```

* For the creation of a new location:

```
python -m cli new_loc -h
```

* For a short period installation:

```
python -m cli short_period -h
```

* For a reorientation study:

```
python -m cli reorientation -h
```

## Demo mode

To use a demonstration of the program, here are some examples of command lines with previously calculated results:

- 2 reorientation for Sherbrooke:

```
python -m cli reorientation -ln Sherbrooke -rn 2
```

- 4 reorientation for Sherbrooke:

```
python -m cli reorientation -ln Sherbrooke -rn 4
```

- Short term installation (10/06 -> 11/08 _%MM%DD format_) in Sherbrooke:

```
python -m cli short_period -ln Sherbrooke -s 10/06 -e 11/08
```

The program creates a png file to visualize the datas and display the results with information messages.

## Parameters

You can change the simulation parameters in the module
[`configuration`](panel_orientation/configuration.py). Especially you can modify the PVLib parameters
to simulate your PV installation. If the PVLib model fits your installation, the power production values will
correspond to your installation. If not, you can still use the program with default values.

To take into account the number of modules, the shade, the variation in the cost of energy,
you can modify the PVLib model in the module [`daily_energy_production`](panel_orientation/daily_energy_production).

You can also change the plot parameters in the module
[`plot_parameters`](panel_orientation/plotter/plot_parameters.py).

## Code structure

The code has been split in different packages:

* [`daily_energy_production`](panel_orientation/daily_energy_production) contains modules related to the creation
  of the database of power production for 1 year and for the defined tilt range.
* [`location_and_panel`](panel_orientation/location_and_panel) contains modules related to the SolarPanelPosition
  and LocationStore entities, which are used to store and read the latitude,
  the longitude and the solar panel configuration.
* [`optimal_tilt`](panel_orientation/optimal_tilt) contains modules related to the optimisation of the tilt angle for a
  given period and a given location.
* [`optimal_dates`](panel_orientation/optimal_dates) contains modules related to the brute force method for
  reorientation studies.
* [`plotter`](panel_orientation/plotter) contains modules related to the plot functions to visualize the simulation
  results of the brute force and the optimal tilt method.
* [`cli`](panel_orientation/cli) contains modules related to the command line interface to visualize
  the simulation results of the brute force and the optimal tilt method.
* [`common`](panel_orientation/common) contains modules related to the useful basic functions

The result files are stored in the folder [`data`](data). The heat_map of daily energy production are stored in the
folder [`year_prod_file`](data/year_prod_file) and the reorientation results are stored in the folder
[`results`](data/results).

## Run tests

To run the created unit-tests, you must run the commands:

```
 python -m unittest discover -s ../tests/unit/ 
```

### Test description

Only unit testing have been performed over the main function related to the optimization algorithm,
with datas calculated for Sherbrooke (Canada) with PVGIS TMY and a generic PVLib Model.

## Syntax validation

To run project syntax validation:

```
 pylint --rcfile=setup.cfg  panel_orientation tests
```

## Thanks

Thanks to David Chuet, Mathieu Blanchard, Maxime Weiss, Erwan Thoux and Antoine Bourassa for their reviews and excellent
advice for all the MR!

Special thanks to Joris Placette for its precious advice concerning the application of the Brute Force Method.

[Packaging structure]: https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure

[packaging guide]: https://packaging.python.org

[package guide #2]: https://www.geeksforgeeks.org/command-line-scripts-python-packaging/

[distribution tutorial]: https://packaging.python.org/tutorials/packaging-projects/

[rst]: http://docutils.sourceforge.net/rst.html

[md]: https://tools.ietf.org/html/rfc7764#section-3.5 "CommonMark variant"

[md use]: https://packaging.python.org/specifications/core-metadata/#description-content-type-optional

