from matplotlib import pylab, cycler

plot_parameters_common = {'font.size': 20,
                          'legend.title_fontsize': 28,
                          'legend.fontsize': 28,
                          'figure.figsize': (16, 9),
                          'figure.titlesize': 32,
                          'figure.labelsize': 28,
                          'axes.labelsize': 28,
                          'axes.titlesize': 32,
                          'axes.prop_cycle': cycler('color', ['black']),
                          'image.cmap': 'plasma',
                          'lines.linewidth': 5,
                          'figure.subplot.left': 0.07,
                          'figure.subplot.right': 0.98,
                          'figure.subplot.bottom': 0,
                          'figure.subplot.top': 0.98,
                          'figure.subplot.wspace': 0.2,
                          'figure.subplot.hspace': 0.2,
                          'figure.dpi': 300,
                          'savefig.format': 'pdf'}


def update_plot_parameters():
    """
    Update the parameters of the plots
    """
    pylab.rcParams.update(plot_parameters_common)
