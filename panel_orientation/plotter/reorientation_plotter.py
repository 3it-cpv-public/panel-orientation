import logging

import matplotlib.pyplot as plt
import numpy as np

from common.consts import YEAR, FIGURE_FOLDER_PATH
from common.file_name_creator import get_reorientation_figure_filename
from common.utils import get_number_of_days_in_year, get_path
from common.utils import get_scale_factor, get_str_date_from_int, get_month_names
from optimal_dates.reorientation_results import ReorientationResults
from plotter.plot_parameters import update_plot_parameters


def plot_multiple_tilt(rs_results: ReorientationResults,
                       show_instruction=True):
    """
    Plot the reorientation study with the reorientation days and the corresponding tilt values
    :param rs_results: Results file from a multiple tilt study
    :param show_instruction: remove the reorientation values for a smaller plot
    """

    update_plot_parameters()

    main_fig, main_axis = plt.subplots()

    # Show the instruction for reorientation (date and tilt value)
    if rs_results.number_of_changes > 1 and show_instruction:
        for date_change in rs_results.get_combination_vector()[:-1]:
            date_change *= get_scale_factor()
            main_axis.annotate(get_str_date_from_int(date_change) + f': {rs_results.shaped_results[date_change + 1]}°',
                               xy=(date_change + 3, rs_results.shaped_results[date_change + 1] + 3),
                               backgroundcolor='white',
                               fontweight='bold')

    # Plot the reorientation graph
    main_axis.plot(rs_results.shaped_results,
                   color='white')

    main_axis.plot(rs_results.shaped_results,
                   label='Tilt Adjsutments',
                   linewidth=2.5)
    data_raw = rs_results.daily_energy_produced.daily_energy_prod_df.transpose()
    color_map = main_axis.pcolormesh(data_raw.columns, data_raw.index, data_raw)
    main_fig.colorbar(color_map,
                      ax=main_axis,
                      label='Daily Energy produced (in Wh)',
                      location='bottom',
                      aspect=40,
                      pad=0.07)

    main_axis.set_ylabel('Tilt Angle (in °)')

    months = get_month_names() + ['']
    main_axis.set_xticks(np.linspace(0, get_number_of_days_in_year(YEAR) - 1, 13), months)
    main_axis.legend()

    fig_path = get_path(FIGURE_FOLDER_PATH,
                        get_reorientation_figure_filename(location=rs_results.location,
                                                          changes_count=rs_results.number_of_changes))

    plt.savefig(fig_path)

    logging.info(f'Figure saved in {fig_path}')
