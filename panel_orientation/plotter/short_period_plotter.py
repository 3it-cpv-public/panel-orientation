import logging

import matplotlib.pyplot as plt
import numpy as np

from common.consts import YEAR, FIGURE_FOLDER_PATH
from common.file_name_creator import get_short_period_figure_filename
from common.utils import get_str_date_from_int, get_number_of_days_in_year, \
    get_month_names, get_path
from optimal_tilt.short_period_results import ShortPeriodResults
from plotter.plot_parameters import update_plot_parameters


def plot_short_period(sp_results: ShortPeriodResults):
    """
    Plot the short period study with daily energy produced database in background
    :param sp_results: results object for a short period study
    """

    update_plot_parameters()

    main_fig, main_axis = plt.subplots()

    # First gray layer:
    gray_db_layer = sp_results.daily_energy_produced.daily_energy_prod_df.transpose()
    main_axis.pcolormesh(gray_db_layer.columns,
                         gray_db_layer.index,
                         gray_db_layer,
                         cmap='gray')

    # Colored layer
    if sp_results.start > sp_results.end:
        first_filtered_data = sp_results.daily_energy_produced[:sp_results.end]
        second_filtered_data = sp_results.daily_energy_produced[sp_results.start:]
        first_filtered_data = first_filtered_data.transpose()
        second_filtered_data = second_filtered_data.transpose()
        main_axis.pcolormesh(first_filtered_data.columns,
                             first_filtered_data.index,
                             first_filtered_data)
        color_map = main_axis.pcolormesh(second_filtered_data.columns,
                                         second_filtered_data.index,
                                         second_filtered_data)

        main_axis.annotate(f'Optimized tilt: {sp_results.best_tilt}°',
                           xy=(3, sp_results.best_tilt + 3),
                           backgroundcolor='white',
                           fontweight='bold')
    else:
        filtered_data = sp_results.daily_energy_produced.daily_energy_prod_df[sp_results.start: sp_results.end]
        filtered_data = filtered_data.transpose()
        color_map = main_axis.pcolormesh(filtered_data.columns, filtered_data.index, filtered_data)
        main_axis.annotate(f'Optimized tilt: {sp_results.best_tilt}°',
                           xy=(np.mean([sp_results.start, sp_results.end]) - 25,
                               sp_results.best_tilt + 3),
                           backgroundcolor='white',
                           fontweight='bold')

    main_fig.colorbar(color_map,
                      ax=main_axis,
                      label='Energy produced (in Wh)',
                      location='bottom',
                      aspect=40,
                      pad=0.07)

    main_axis.plot(sp_results.get_shaped_results())

    for date in (sp_results.start, sp_results.end):
        main_axis.annotate(get_str_date_from_int(date),
                           xy=(date - 8, main_axis.get_ylim()[0] + 2),
                           backgroundcolor='white',
                           fontweight='bold')

    main_axis.set_ylabel('Tilt orientation (in °)')

    months = get_month_names() + ['']
    main_axis.set_xticks(np.linspace(0, get_number_of_days_in_year(YEAR) - 1, 13), months)

    logging.info(sp_results)

    fig_path = get_path(FIGURE_FOLDER_PATH, get_short_period_figure_filename(sp_results.location,
                                                                             sp_results.start,
                                                                             sp_results.end))

    plt.savefig(fig_path)

    logging.info(f'Figure saved in {fig_path}')
