import os

from pandas import DataFrame, read_csv
from pvlib.location import Location

from common.consts import RESULT_FOLDER_PATH, PERIOD_ARRAY
from common.file_name_creator import get_result_filename
from common.utils import get_path
from optimal_dates.period_optimizer import \
    find_best_period


def get_orientation_records(location: Location,
                            number_of_changes: int,
                            result_folder_path=RESULT_FOLDER_PATH) -> DataFrame:
    """
    Read a one-year orientation file for the chosen location and the chosen number of reorientation
    If the file does not exist, the function create it
    :param number_of_changes: number of reorientation of the PV panel
    :param location: location of the study
    :param result_folder_path: path where results are stored
    :return: best tilt and best dates of reorientation for the chosen parameters
    """

    result_file_name = get_result_filename(location=location,
                                           changes_count=number_of_changes)

    complete_path = get_path(folder_path=result_folder_path, file_name=result_file_name)

    if not os.path.isfile(complete_path):
        find_best_period(location=location,
                         changes_count=number_of_changes)

    dates_and_tilts_result = read_csv(complete_path, index_col=0)
    dates_and_tilts_result = dates_and_tilts_result.drop(PERIOD_ARRAY)

    return dates_and_tilts_result
