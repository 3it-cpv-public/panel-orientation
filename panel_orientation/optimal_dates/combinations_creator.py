import itertools as it

import numpy as np
from pandas import DataFrame
from more_itertools import pairwise

from common.consts import COMBINATION_VECTOR, PERIOD_ARRAY
from common.consts import YEAR
from common.utils import get_number_of_days_in_year
from configuration import PERIOD_RESOLUTION


def get_combinations(changes_count: int, period_resolution=PERIOD_RESOLUTION) -> DataFrame:
    """
    This function creates a dataframe of all the possible combination of date of change over a year
    :param changes_count: number of re-orientation over a year
    :param period_resolution: resolution of the period
    :return: dataframe of each period table
    """
    if changes_count <= 1:
        raise ValueError('Reorientation number invalid!\n'
                         'Use the short period mode to find a constant tilt value.')
    period_resolution = max(changes_count, period_resolution)

    scale_factor = get_number_of_days_in_year(YEAR) // period_resolution

    # Create the DataFrame
    part_list = DataFrame(
        {COMBINATION_VECTOR: list(it.combinations(range(1, period_resolution + 1), changes_count))})

    def from_combinations_to_period(raw_combinations: tuple) -> np.array:
        """
        This returns the period array for each raw combination : [[0, d1], [d1, d2] ... [dn, 365]]
        It converts the raw_combination list in an array of date range for the future calculation
        :param raw_combinations: list which contains each period size [d1, d2 - d1, d3 - d2, ... , dn - dn-1]
        :return: period array
        """
        return np.array([[i, j] for i, j in pairwise(it.chain((0,), raw_combinations, (period_resolution,)))])

    part_list[PERIOD_ARRAY] = part_list.apply(
        lambda x: from_combinations_to_period(x[COMBINATION_VECTOR]) * scale_factor, axis=1)

    return part_list
