import logging

from pandas import DataFrame, Series
from pvlib.location import Location

from common.consts import TILT_POWER_ARRAY, POWER_PRODUCED, PERIOD_ARRAY, RESULT_FOLDER_PATH
from common.file_name_creator import get_result_filename
from common.progress_counter import ProgressCounter
from common.utils import get_path
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from optimal_dates.combinations_creator import \
    get_combinations
from optimal_tilt.tilt_and_power_finder import \
    get_best_tilt_and_power_from_period_vector


def find_best_period(location: Location, changes_count: int):
    """
    This function find the best period array to get the maximum of energy produced for one year
    with N changes of orientation
    :param location: localisation of the study
    :param changes_count: number of date of changes of the panel orientation
    """

    production_database = DailyEnergyProducedReader(location)

    logging.info(f'Calculating orientation in: {location.name}\n'
                 f'Changes : {changes_count}')

    # Creating the period dataframe
    period_split = get_combinations(changes_count=changes_count)

    bf_counter = ProgressCounter(len(period_split))

    # Add new columns to the dataframe
    period_split[[TILT_POWER_ARRAY, POWER_PRODUCED]] = period_split[PERIOD_ARRAY].apply(
        lambda x: Series(get_best_tilt_and_power_from_period_vector(production_database,
                                                                    x,
                                                                    bf_counter=bf_counter)))

    interesting_index = period_split[POWER_PRODUCED].idxmax()

    result_file_name = get_result_filename(location=location,
                                           changes_count=changes_count)

    complete_path = get_path(folder_path=RESULT_FOLDER_PATH, file_name=result_file_name)

    DataFrame(period_split.loc[interesting_index]).to_csv(complete_path)

    logging.info(f'File exported in {complete_path}')
