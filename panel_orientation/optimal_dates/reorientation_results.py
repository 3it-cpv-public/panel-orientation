import ast

from pvlib.location import Location

from common.consts import COMBINATION_VECTOR, TILT_POWER_ARRAY, POWER_PRODUCED
from common.utils import get_scale_factor
from configuration import PERIOD_RESOLUTION
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from optimal_dates.orientation_file_reader import get_orientation_records


class ReorientationResults:
    """
    Entity to access the results of a reorientation study
    """

    def __init__(self,
                 location: Location,
                 number_of_changes: int):
        self.location = location
        self.number_of_changes = number_of_changes
        self.period_resolution = max(number_of_changes, PERIOD_RESOLUTION)
        self.daily_energy_produced = DailyEnergyProducedReader(location=location)
        self.result_raw = get_orientation_records(location=location,
                                                  number_of_changes=number_of_changes)
        self.shaped_results = self.get_shaped_result_for_plotting()
        self.annual_energy_produced = float(self.result_raw.loc[POWER_PRODUCED][0])

    def __str__(self):
        return f'Annual energy produced : {self.annual_energy_produced}\n Wh' \
               f'Combination vector : {self.get_combination_vector()}'

    def get_combination_vector(self):
        return list(ast.literal_eval(self.result_raw.loc[COMBINATION_VECTOR][0])) + [self.period_resolution]

    def get_tilt_vector(self):
        actual_tilt_vector = ast.literal_eval(self.result_raw.loc[TILT_POWER_ARRAY][0])
        return [tilt_and_power[0] for tilt_and_power in actual_tilt_vector] + [actual_tilt_vector[0][0]]

    def get_shaped_result_for_plotting(self) -> list:
        """
        This function adapts the values given by the orientation dataframe before plotting
        :return: list of reoriented tilt for each date
        """
        reoriented_tilt = []

        period = 0

        for i, value in enumerate(self.get_tilt_vector()):
            # The while loop is needed to change the tilt value at each reorientation date
            while period < self.get_combination_vector()[i]:
                for _ in range(get_scale_factor()):
                    reoriented_tilt.append(value)
                period += 1
        return reoriented_tilt
