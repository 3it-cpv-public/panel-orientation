class ProgressCounter:
    """
    Counter used to print the % of calculation done for a brute force calculation
    """

    def __init__(self, total_partition_number: int, print_progress=True):
        """
        :param total_partition_number: size of the list which is iterated
        """
        self.count = 0
        self.total_count = total_partition_number
        self.print_progress = print_progress

    def increment(self):
        self.count += 1
        if self.print_progress:
            temp_run = self.count / self.total_count
            print(f'\r{round(temp_run * 100, 2)} %  ', end='', flush=True)
            if self.count == self.total_count:
                print('\r')
