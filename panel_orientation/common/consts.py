from configuration import FORECAST_MODEL, PVGIS

# Indexing
START_DATE = 'start_date'
END_DATE = 'end_date'
TILT_ANGLE = 'tilt_angle'
AVG_INCIDENCE = 'avg_incidence'
COMBINATION_VECTOR = 'combination_vector'
PERIOD_ARRAY = 'period_array'
TILT_POWER_ARRAY = 'tilt_power_array'
POWER_PRODUCED = 'power_produced'
SHORT_PERIOD = 'short_period'
REORIENTATION = 'reorientation'
NEW_LOC = 'new_loc'

# TMY API Configuration
PVGIS_API_URL = 'https://re.jrc.ec.europa.eu/api/v5_2/'
YEAR = 2022

# Folders and files PATH
# Results
if FORECAST_MODEL == PVGIS:
    PROD_FOLDER_PATH = '../data/year_prod_file/tmy_pvgis/'
    RESULT_FOLDER_PATH = '../data/results/tmy_pvgis/'
else:
    PROD_FOLDER_PATH = '../data/year_prod_file/clear_sky/'
    RESULT_FOLDER_PATH = '../data/results/clear_sky/'

# Location
LOCATION_FOLDER_PATH = '../data/'
LOCATION_FILE_NAME = 'stored_location.csv'

# Test
TEST_PROD_FOLDER_PATH = '../tests/data_tests/year_prod_file'
TEST_RESULT_FOLDER_PATH = '../tests/data_tests/results'
TEST_LOCATION_FOLDER_PATH = '../tests/data_tests/'

# Figure
FIGURE_FOLDER_PATH = '../data/plotted_figures/'
