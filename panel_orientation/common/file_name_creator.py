from pvlib.location import Location

from common.utils import get_str_date_from_int
from configuration import PERIOD_RESOLUTION


def get_production_filename(location: Location) -> str:
    """
    Return the file name of a stored csv file
    :param location: Localisation
    :return: file name
    """
    return f'daily_energy_production_loc_{location.name}.csv'


def get_result_filename(location: Location,
                        changes_count: int) -> str:
    """
    Return the file name of a stored csv file
    :param changes_count: number of dates of change
    :param location: Localisation
    :return: file name
    """
    period_rez = max(changes_count, PERIOD_RESOLUTION)
    return f'reorientation_study_loc_{location.name}_N_{changes_count}_period_res_{period_rez}.csv'


def get_reorientation_figure_filename(location: Location, changes_count: int):
    """
    Get the figure name (results) for a reorientation study
    """
    period_rez = max(changes_count, PERIOD_RESOLUTION)
    return f'reorientation_loc_{location.name}_N_{changes_count}_period_res_{period_rez}.pdf'


def get_short_period_figure_filename(location: Location,
                                     start: int,
                                     end: int):
    """
    Get the figure name (results) for a short period study
    """

    return f'reorientation_loc_{location.name}_start_' \
           f'{get_str_date_from_int(start)}_end_{get_str_date_from_int(end)}.pdf'
