import calendar
import os
from datetime import datetime, date, timedelta
from pathlib import Path
from typing import List

from common.consts import YEAR
from configuration import PERIOD_RESOLUTION


def get_number_of_days_in_year(year: int) -> int:
    """
    Find the number of days in the given year
    :param year: Year of the study
    :return: number of days in one year
    """
    return (date(year + 1, 1, 1) - date(year, 1, 1)).days


def get_int_from_date_str(date_str: str) -> int:
    """
    Convert the date entered by the user into an integer
    :param date_str: Date as string with format "%m/%d"
    :return: date in integer format
    """
    current_date = datetime.strptime(date_str + '/' + str(YEAR), "%m/%d/%Y")
    day_ref = datetime(year=current_date.year, month=1, day=1)
    return (current_date - day_ref).days


def get_str_date_from_int(date_integer: int) -> str:
    """
    Create a date format like 'Feb 15' from integer in [1 : 365]
    :param date_integer: date to convert
    :return: str of this date
    """
    start = datetime(year=YEAR, month=1, day=1)
    actual_datetime = start + timedelta(days=date_integer)
    return actual_datetime.strftime("%d %b")


def get_scale_factor(year=YEAR, period_resolution=PERIOD_RESOLUTION) -> int:
    """
    This calculates the size of one period in function of the chosen period resolution
    :return: size of the period
    """
    return get_number_of_days_in_year(year) // period_resolution


def get_tilt_table(latitude: float, tilt_resolution: int) -> list:
    """
    Create the tilt range from the latitude of the chosen_location
    :param latitude: latitude of the chosen_location
    :param tilt_resolution: resolution of the tilt table in degree
    :return: tilt table
    """
    tilt_center = abs(int(latitude))
    tilt_min = tilt_center - 45
    tilt_max = tilt_center + 45 + tilt_resolution

    # Reduce the interval (If the normal of the panel is below the horizon, it can't be an optimal position)
    tilt_min = max(tilt_min, -90)
    tilt_max = min(tilt_max, 90)

    return list(range(tilt_min, tilt_max, tilt_resolution))


def get_month_names() -> List[str]:
    """
    Get calendar month names
    :return: List of month names
    """
    return [month[:3] for month in list(calendar.month_name[1:])]


def get_path(folder_path: str, file_name: str) -> Path:
    """
    Get complete path of orientation, production or localisation files
    :param folder_path: file path
    :param file_name: file name
    """
    parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    directory_path = os.path.join(parent_dir, folder_path)

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    return Path(os.path.join(directory_path, file_name))


def get_copyright() -> str:
    """
    Read the licence name (1st line of the LICENCE.txt file)
    """
    licence_name = 'LICENSE.txt'
    with open(get_path('../', licence_name), 'r', encoding="utf-8") as licence_file:
        licence_name = licence_file.readline()
    return licence_name
