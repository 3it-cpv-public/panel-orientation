from pvlib.location import Location

from common.consts import YEAR, PROD_FOLDER_PATH
from common.utils import get_number_of_days_in_year
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from optimal_tilt.tilt_and_power_finder import get_best_tilt_and_power_from_period


class ShortPeriodResults:
    """
    Entity to access the results of a short period study
    """

    def __init__(self, location: Location, start: int, end: int, prod_folder_path=PROD_FOLDER_PATH):
        """
        :param location: location of the study
        :param start: beginning of the period
        :param end: end of the period
        """
        self.location = location
        self.start = start
        self.end = end
        self.daily_energy_produced = DailyEnergyProducedReader(location=location, prod_folder_path=prod_folder_path)
        self.sp_res_value = get_best_tilt_and_power_from_period(daily_energy_prod=self.daily_energy_produced,
                                                                start=self.start,
                                                                end=self.end)

        self.best_tilt = self.sp_res_value[0]
        self.total_energy = self.sp_res_value[1]

    def __str__(self):
        return f'Optimal tilt : {self.best_tilt}\n Energy produced : {round(self.total_energy)} Wh'

    def get_shaped_results(self) -> list:
        """
        Create a table of None values outside the chosen period and best_tilt in the period
        :return: table of tilt value covering one year
        """
        first_period = self.start > self.end
        tilt_to_plot = []
        for i in range(get_number_of_days_in_year(YEAR)):
            if first_period:
                if i < self.end or self.start <= i:
                    tilt_to_plot.append(self.best_tilt)
                else:
                    tilt_to_plot.append(None)
            else:
                if self.start <= i < self.end:
                    tilt_to_plot.append(self.best_tilt)
                else:
                    tilt_to_plot.append(None)

        return tilt_to_plot
