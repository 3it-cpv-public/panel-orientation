import numpy as np
from pandas import DataFrame

from common.consts import TILT_ANGLE, POWER_PRODUCED
from common.progress_counter import ProgressCounter
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader


def get_best_tilt_and_power_from_period(daily_energy_prod: DailyEnergyProducedReader,
                                        start: int,
                                        end: int) -> tuple:
    """
    This function finds the best tilt and calculates the production for a given period
    :param daily_energy_prod: daily energy production object
    :param start: start of the period (int between 0 and 364)
    :param end: end of the period (int between 0 and 364)
    (the day 0 is not a day of change)
    :return: tuple of the best tilt and associated energy production
    """

    tilt_and_power = DataFrame({TILT_ANGLE: daily_energy_prod.tilt_table}, dtype=int)

    # Calculates the power produced for each value of tilt angle
    tilt_and_power[POWER_PRODUCED] = tilt_and_power.apply(
        lambda x: daily_energy_prod.get_energy_from_tilt_and_period(tilt_angle=x[TILT_ANGLE], start=start, end=end),
        axis=1)
    max_index = tilt_and_power[POWER_PRODUCED].idxmax()

    return tilt_and_power[TILT_ANGLE][max_index], tilt_and_power[POWER_PRODUCED][max_index]


def get_best_tilt_and_power_from_period_vector(production_database: DailyEnergyProducedReader,
                                               period_array: np.array,
                                               bf_counter: ProgressCounter) -> list:
    """
    This function converts the period array in tilt and power values
    :param production_database: energy production data
    :param period_array: list of [start, end] couples
    :param bf_counter: counter to print the % of the calculation done
    :return: best tilt for each period and the total power produced with the given period array in a list
    """
    bf_counter.increment()

    tilt_and_power_array = []

    # First period calculation
    temp_start = period_array[-1][0]
    temp_end = period_array[0][1]
    first_temp_result = get_best_tilt_and_power_from_period(production_database,
                                                            temp_start,
                                                            temp_end)
    tilt_and_power_array.append(first_temp_result)
    total_power = first_temp_result[1]

    for period in period_array[1:-1]:  # Remove the first and the last period already calculated before
        temp_start = period[0]
        temp_end = period[1]

        temp_tilt_and_power = get_best_tilt_and_power_from_period(production_database,
                                                                  temp_start,
                                                                  temp_end)

        total_power += temp_tilt_and_power[1]
        tilt_and_power_array.append(temp_tilt_and_power)

    return [tilt_and_power_array, total_power]


def get_annual_best_tilt(production_database: DailyEnergyProducedReader) -> tuple:
    """
    Get the annual best tilt (fixed panel)
    :param production_database: energy production data
    :return: annual optimal tilt and power produced at this tilt angle
    """
    tilt_and_power = DataFrame({TILT_ANGLE: production_database.tilt_table}, dtype=int)

    # Calculates the power produced for each value of tilt angle
    tilt_and_power[POWER_PRODUCED] = tilt_and_power.apply(
        lambda x: production_database.get_annual_energy_produced_from_tilt(tilt_angle=x[TILT_ANGLE]),
        axis=1)
    max_index = tilt_and_power[POWER_PRODUCED].idxmax()
    return tilt_and_power[TILT_ANGLE][max_index], tilt_and_power[POWER_PRODUCED][max_index]
