import argparse
import logging

from common.consts import SHORT_PERIOD, REORIENTATION, NEW_LOC
from common.utils import get_copyright
from cli.parser_utils import create_subparser_args


def main():
    """
    Main function of the application
    """

    main_parser = argparse.ArgumentParser(description='Main parameters',
                                          epilog=get_copyright())
    subparser = main_parser.add_subparsers(required=True,
                                           help='Features available :')

    for mode in [SHORT_PERIOD, REORIENTATION, NEW_LOC]:
        create_subparser_args(mode, subparser)

    main_args = main_parser.parse_args()

    main_args.func(main_args)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
