import argparse
import logging

from common.consts import SHORT_PERIOD, REORIENTATION, NEW_LOC
from common.utils import get_int_from_date_str
from location_and_panel.location_store import LocationStore
from optimal_tilt.short_period_results import ShortPeriodResults
from plotter.reorientation_plotter import plot_multiple_tilt
from plotter.short_period_plotter import plot_short_period
from optimal_dates.reorientation_results import ReorientationResults


def get_location_name_from_parser(sub_parser: argparse.ArgumentParser):
    """
    Create the location name argument in a sub_parser
    :param sub_parser: sub_parser where the loc name parameter is added
    """

    sub_parser.add_argument("-ln", "--location_name", type=str, required=True,
                            help='It is a case sensitive parameter.')


def create_subparser_args(mode: str, subparser_creator):
    """
    Add the arguments for each subparser
    :param mode: name of the MODE to create
    :param subparser_creator: additional parser creator
    """

    if mode == SHORT_PERIOD:
        short_period_subparser = \
            subparser_creator.add_parser('short_period', help='When a panel is installed for a short period of time')

        short_period_subparser.set_defaults(func=run_short_period)

        get_location_name_from_parser(short_period_subparser)

        short_period_subparser.add_argument('-s', '--start', type=get_int_from_date_str, required=True,
                                            help='Installation date, must be like "MM/DD"')
        short_period_subparser.add_argument('-e', '--end', type=get_int_from_date_str, required=True,
                                            help='Uninstallation date, must be like "MM/DD"')

    elif mode == REORIENTATION:
        reorientation_subparser = subparser_creator.add_parser('reorientation',
                                                               help='When the installation is for 1 year or more')
        reorientation_subparser.set_defaults(func=run_reorientation)

        get_location_name_from_parser(reorientation_subparser)

        reorientation_subparser.add_argument("-rn", "--reorientation_number", type=int, required=True,
                                             help='Number of reorientation requested')

    elif mode == NEW_LOC:
        location_subparser = \
            subparser_creator.add_parser('new_loc',
                                         help='First step: create a new panel location if it does not exist already')

        location_subparser.set_defaults(func=add_location)

        get_location_name_from_parser(location_subparser)

        location_subparser.add_argument('-lat', '--latitude', type=float, required=True,
                                        help='Panel_s latitude location')
        location_subparser.add_argument('-lon', '--longitude', type=float, required=True,
                                        help='Panel_s longitude location')
    else:
        logging.error(f'Mode {mode} not supported!')


def run_short_period(args: argparse.Namespace):
    """
    Plot the result of the short period study
    :param args: user input arguments
    """

    sp_results = ShortPeriodResults(location=LocationStore().get_pv_lib_location(args.location_name),
                                    start=args.start,
                                    end=args.end)
    plot_short_period(sp_results)


def run_reorientation(args: argparse.Namespace):
    """
    Plot the result of the short period study
    :param args: user input arguments
    """
    rs_results = ReorientationResults(location=LocationStore().get_pv_lib_location(args.location_name),
                                      number_of_changes=args.reorientation_number)
    logging.info(str(rs_results))
    plot_multiple_tilt(rs_results)


def add_location(args: argparse.Namespace):
    """
    Add the location to the database
    :param args: user input arguments
    """
    if LocationStore().is_location_in_store(args.location_name):
        logging.warning(f'The location name {args.location_name} already exists ! \n'
                        f'{LocationStore().get_pv_lib_location_info(args.location_name)}\n'
                        f'You can now select a simulation MODE: short period or reorientation')
    else:
        LocationStore().add_a_location(location_name=args.location_name,
                                       lat=args.latitude,
                                       lon=args.longitude)
