from pvlib.location import Location


class SolarPanelPosition:
    """
    Describe the solar panel
    """

    def __init__(self, pv_lib_loc: Location, tilt_angle: float, panel_azimuth_angle=None):
        """
        Set the panel entity
        :param pv_lib_loc: location of the panel
        :param tilt_angle: tilt angle chosen
        """

        # Panel orientation is defined as :
        # - tilt_table values range from 0 to 90°
        # - azimuth set to 0° or 180° depending on the hemisphere

        if panel_azimuth_angle is None:

            if pv_lib_loc.latitude > 0:
                self.panel_azimuth_angle = 180
            else:
                self.panel_azimuth_angle = 0
        else:
            self.panel_azimuth_angle = panel_azimuth_angle

        self.panel_tilt_angle = tilt_angle

    def __str__(self):
        return f'Tilt angle : {self.panel_tilt_angle}' \
               f'Azimuth angle : {self.panel_azimuth_angle}'

    def increment_tilt_angle(self, step_tilt: float):
        self.panel_tilt_angle += step_tilt
