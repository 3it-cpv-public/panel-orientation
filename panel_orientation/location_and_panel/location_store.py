import logging

from pandas import read_csv
from pvlib.location import Location

from common.consts import LOCATION_FOLDER_PATH, LOCATION_FILE_NAME
from common.utils import get_path


class LocationStore:
    """
    This class contains all the tools needed to add a location to the database and to read the database
    """

    def __init__(self, location_folder_path=LOCATION_FOLDER_PATH):
        """
        Build
        :param location_folder_path: path of the location folder
        """
        complete_path = get_path(location_folder_path, LOCATION_FILE_NAME)
        self.location_database = read_csv(
            complete_path,
            index_col='location')
        self.location_folder_path = location_folder_path

    def is_location_in_store(self, location_name: str) -> bool:
        return location_name in self.location_database.columns

    def add_a_location(self, location_name: str, lat: float, lon: float):
        """
        Used to add a location in the correct format
        :param location_name: name of the new location
        :param lat: latitude of the new location
        :param lon: longitude of the new location
        """

        # add the new column to the existing csv
        self.location_database[location_name] = [lat, lon]
        self.location_database.to_csv(get_path(self.location_folder_path, LOCATION_FILE_NAME))

        logging.info('New location has been created:\n'
                     f'Location name: {location_name}\n'
                     f'Latitude: {lat}\n'
                     f'Longitude: {lon}')

    def get_pv_lib_location(self, location_name: str) -> Location:
        """
        Get the location object from the database of lat and lon saved, using the location name as a Key
        :param location_name: name of the location
        """
        if not self.is_location_in_store(location_name):
            raise ValueError('Location does not exist in the database! \n'
                             'Please use the new_loc function to create it before running the simulation')
        location_infos = self.location_database[location_name]
        return Location(latitude=location_infos['lat'],
                        longitude=location_infos['lon'],
                        name=location_name)

    def get_pv_lib_location_info(self, location_name: str) -> str:
        pv_lib_loc = self.get_pv_lib_location(location_name)
        return f'name : {pv_lib_loc.name}\n' \
               f'lat : {pv_lib_loc.latitude}\n' \
               f'lon : {pv_lib_loc.longitude}\n'
