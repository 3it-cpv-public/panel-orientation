import logging
from datetime import datetime

import pvlib
import pytz
from pandas import DataFrame, date_range
from pandas import Timestamp
from pvlib.location import Location

from common.consts import YEAR, PVGIS_API_URL
from configuration import PVGIS, CLEAR_SKY


class Forecast:
    """
    This is a forecast object to store and uniformize the weather database collected with different tools
    """

    def __init__(self, method_name: str, location: Location):
        """
        :param method_name: method used to calculate the forecast
        :param location: Location of the created forecast entity
        """
        self.method_name = method_name
        self.location = location
        if method_name == PVGIS:
            self.one_year_forecast = self.get_hourly_tmy_data_from_pvgis()
        elif method_name == CLEAR_SKY:
            self.one_year_forecast = self.get_hourly_clear_sky_from_pvlib()
        else:
            logging.warning('Wrong method name')

    def get_hourly_tmy_data_from_pvgis(self) -> DataFrame:
        """
        Use the API provided by PVGIS to download TMY datas
        :return: Irradiance and temperature values by hours for one year
        """
        tmy_data = pvlib.iotools.get_pvgis_tmy(self.location.latitude,
                                               self.location.longitude,
                                               map_variables=True,
                                               url=PVGIS_API_URL)[0]

        def reset_year(ts_value: Timestamp):
            return ts_value.replace(year=YEAR)

        tmy_data.index = tmy_data.index.map(reset_year)

        return tmy_data

    def get_hourly_clear_sky_from_pvlib(self) -> DataFrame:
        """
        Use the clear sky model integrated in PVLib
        :return: Irradiance datas from clear sky model
        """
        pvlib_location = Location(latitude=self.location.latitude,
                                  longitude=self.location.longitude)

        clear_sky = pvlib_location.get_clearsky(date_range(start=f'{YEAR}-01-01',
                                                           end=f'{YEAR}-12-31 23:00:00',
                                                           freq='H',
                                                           tz=pytz.UTC))
        return clear_sky

    def get_forecast_for_one_period(self, start: datetime, end: datetime) -> DataFrame:
        """
        Select one period of the annual forecast database
        :param start: start of the period
        :param end: end of the period
        """
        return self.one_year_forecast[start:end]
