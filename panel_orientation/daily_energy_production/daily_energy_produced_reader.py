import os

from pandas import DataFrame, read_csv
from pvlib.location import Location

from common.consts import PROD_FOLDER_PATH
from common.file_name_creator import get_production_filename
from common.utils import get_path, get_tilt_table
from configuration import TILT_RESOLUTION
from daily_energy_production.library_creator \
    import export_production_records


class DailyEnergyProducedReader:
    """
    Entity to access daily production energy
    """

    def __init__(self,
                 location: Location,
                 tilt_resolution=TILT_RESOLUTION,
                 prod_folder_path=PROD_FOLDER_PATH):
        """
        :param location: location of the database
        :param tilt_resolution: tilt resolution used to read the database during brute force method
        :param prod_folder_path: path of the daily energy production csv files
        """
        self.location = location
        self.tilt_table = get_tilt_table(self.location.latitude, tilt_resolution)
        self.daily_energy_prod_df = self.get_production_records(prod_folder_path)

    def get_production_records(self, prod_folder_path: str) -> DataFrame:
        """
        Read or create and read a one-year production file for the chosen location
        :param prod_folder_path: path of the daily energy production csv files
        :return: energy production value for each tilt angle and for each day in Wh
        """

        file_name = get_production_filename(self.location)
        complete_path = get_path(prod_folder_path, file_name)

        if not os.path.isfile(complete_path):
            export_production_records(self.location)

        data_raw = read_csv(complete_path, dtype=float)

        del data_raw[data_raw.columns[0]]
        data_raw.columns = data_raw.columns.astype(dtype=int)

        return data_raw

    def get_energy_from_tilt_and_period(self, tilt_angle: int, start: int, end: int) -> float:
        """
        Read and sum the energy produced for a specific tilt and period
        :param tilt_angle: tilt angle of the panel
        :param start: start of the period
        :param end: end of the period
        :return: energy produced during the period (in Wh)
        """
        first_period = start > end
        if first_period:
            return sum(self.daily_energy_prod_df[tilt_angle][:end]) + sum(
                self.daily_energy_prod_df[tilt_angle][start:])
        return sum(self.daily_energy_prod_df[tilt_angle][start:end])

    def get_annual_energy_produced_from_tilt(self, tilt_angle: int) -> float:
        """
        Get the annual energy produced for a specific tilt
        :param tilt_angle: tilt angle of the panel
        :return: annual energy produced (in Wh)
        """
        return sum(self.daily_energy_prod_df[tilt_angle][:])
