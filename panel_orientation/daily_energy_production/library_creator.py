import logging
from datetime import timedelta, datetime

import pytz
from pandas import DataFrame
from pvlib.location import Location

from common.consts import PROD_FOLDER_PATH, YEAR, FORECAST_MODEL
from common.file_name_creator import get_production_filename
from common.progress_counter import ProgressCounter
from common.utils import get_number_of_days_in_year, get_path
from common.utils import get_tilt_table
from daily_energy_production.forecast import Forecast
from daily_energy_production.pv_panel_simulator import get_model_results_from_pvlib, \
    get_dc_energy_produced
from location_and_panel.solar_panel_position import SolarPanelPosition


def export_production_records(chosen_location: Location,
                              tilt_resolution=1):
    """
    This function creates and exports the database of the energy production records for each day of the year and each
    tilt angle thanks to the PVlib library
    :param tilt_resolution: resolution of the tilt angle parameter in degrees
    :param chosen_location: location of the study
    """

    tilt_table = get_tilt_table(chosen_location.latitude, tilt_resolution)

    logging.info(f'Calculating production records in: {chosen_location.name}\n'
                 f'Tilt range is {tilt_table[0]} -> {tilt_table[-1]}')

    day_table = list(range(get_number_of_days_in_year(YEAR)))
    prod_progress_counter = ProgressCounter(len(day_table))
    df_production_for_each_day_tilt = DataFrame(index=day_table, columns=tilt_table)

    one_year_forecast = Forecast(FORECAST_MODEL, location=chosen_location)

    start = datetime(year=YEAR, month=1, day=1, tzinfo=pytz.UTC)

    for n_day in day_table:

        prod_progress_counter.increment()

        end = start + timedelta(days=1)
        daily_forecast = one_year_forecast.get_forecast_for_one_period(start, end)

        for tilt_angle in tilt_table:
            solar_panel = SolarPanelPosition(chosen_location, tilt_angle)

            temp_model = get_model_results_from_pvlib(chosen_location,
                                                      solar_panel,
                                                      daily_forecast)

            df_production_for_each_day_tilt[tilt_angle][n_day] = get_dc_energy_produced(temp_model)
        start = end

    file_name = get_production_filename(chosen_location)
    complete_path = get_path(PROD_FOLDER_PATH, file_name)

    df_production_for_each_day_tilt.to_csv(complete_path)
    logging.info(f'File exported in {complete_path}')
