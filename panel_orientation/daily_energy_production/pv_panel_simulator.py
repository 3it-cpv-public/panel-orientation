from pandas import DataFrame
from pvlib.location import Location
from pvlib.modelchain import ModelChain, ModelChainResult
from pvlib.pvsystem import PVSystem

from configuration import MODULE_PARAMETERS, TEMPERATURE_MODEL, INVERTER_PARAMETERS, \
    AOI_MODEL, SPECTRAL_MODEL
from location_and_panel.solar_panel_position import SolarPanelPosition


def get_model_results_from_pvlib(chosen_location: Location,
                                 solar_panel: SolarPanelPosition,
                                 forecast: DataFrame) -> ModelChainResult:
    """
    This function uses the library PVlib to find the energy produced for a tilt orientation and a chosen day of the year
    :param forecast: ghi, dni and dhi and other weather values
    :param solar_panel: solar panel position for the day and tilt
    :param chosen_location: position on the earth
    :return: total energy produced in Wh
    """

    pv_system = PVSystem(
        module_parameters=MODULE_PARAMETERS,
        inverter_parameters=INVERTER_PARAMETERS,
        surface_tilt=solar_panel.panel_tilt_angle,
        surface_azimuth=solar_panel.panel_azimuth_angle,
        temperature_model_parameters=TEMPERATURE_MODEL)

    model = ModelChain(system=pv_system,
                       location=chosen_location,
                       aoi_model=AOI_MODEL,
                       spectral_model=SPECTRAL_MODEL)

    model.run_model(forecast)

    return model.results


def get_dc_energy_produced(model_res: ModelChainResult) -> float:
    """
    Sum the hourly power values calculated by the PVLib model
    :param model_res: Results provided by PVlib model
    """
    return model_res.dc.sum()
