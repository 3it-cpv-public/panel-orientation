import logging

import numpy as np
from matplotlib import pyplot as plt, ticker, pylab

from common.consts import YEAR
from common.utils import get_month_names, get_number_of_days_in_year, get_path
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from paper_results_and_figures.consts import PAPER_FIGURE_FOLDER_PATH
from plotter.plot_parameters import update_plot_parameters


def plot_daily_energy(locations: list):
    """
    Plot the reorientation study with the reorientation days and the corresponding tilt values
    :param locations: names of the 4 cities
    """
    update_plot_parameters()
    pylab.rcParams.update({'figure.figsize': (16, 12),
                           'figure.subplot.left': 0.07,
                           'figure.subplot.right': 1,
                           'figure.subplot.bottom': 0.05,
                           'figure.subplot.top': 0.95,
                           'figure.subplot.wspace': 0.1,
                           'figure.subplot.hspace': 0.11})

    months = get_month_names() + ['']

    main_fig, axes = plt.subplots(2, 2, sharex='all')

    energy_min, energy_max = float("inf"), float("-inf")

    for selected_axis, i_loc in zip(axes.flat, range(4)):
        chosen_loc = LocationStore().get_pv_lib_location(locations[i_loc])

        data_raw = DailyEnergyProducedReader(chosen_loc).daily_energy_prod_df.transpose()

        energy_min = min(energy_min, data_raw.min().min())
        energy_max = max(energy_max, data_raw.max().max())

        color_map = selected_axis.pcolormesh(data_raw.columns, data_raw.index, data_raw, vmin=energy_min,
                                             vmax=energy_max)
        selected_axis.set_title(chosen_loc.name)
        selected_axis.set_xticks(np.linspace(0, get_number_of_days_in_year(YEAR) - 1, 13), months)

    energy_cb = main_fig.colorbar(color_map,
                                  ax=axes,
                                  location='right',
                                  aspect=70,
                                  pad=0.03)

    ticks = ticker.FuncFormatter(lambda x, pos: f'{round(x * 1 / 1000, 2)}')
    energy_cb.ax.yaxis.set_major_formatter(ticks)

    energy_cb.set_label(label='Daily Energy Produced (in kWh)')

    main_fig.supylabel("Tilt Orientation (in °)", fontsize=28)

    fig_path = get_path(PAPER_FIGURE_FOLDER_PATH, 'daily_energy_for_4_loc.pdf')
    plt.savefig(fig_path)

    logging.info(f'Figure saved in {fig_path}')
