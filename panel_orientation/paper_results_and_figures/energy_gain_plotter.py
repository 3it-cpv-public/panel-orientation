import logging

from matplotlib import pyplot as plt
from pandas import DataFrame

from common.utils import get_path
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from optimal_dates.reorientation_results import ReorientationResults
from optimal_tilt.tilt_and_power_finder import get_annual_best_tilt
from paper_results_and_figures.consts import PAPER_FIGURE_FOLDER_PATH
from plotter.plot_parameters import update_plot_parameters


def plot_gain_study_multiple_locations(location_table: list,
                                       table_number_of_changes: list):
    """
    Plot the gain of production for each number of changes given in the table
    :param location_table: list of the location names of the study
    :param table_number_of_changes: list of desired number of changes
    """

    update_plot_parameters()

    table_number_of_changes.append(365)
    new_table_to_plot = [1] + table_number_of_changes
    df_to_plot = DataFrame(index=new_table_to_plot, columns=location_table)

    for location_name in location_table:
        location = LocationStore().get_pv_lib_location(location_name)
        production_database = DailyEnergyProducedReader(location=location)
        _, max_energy = tuple(get_annual_best_tilt(production_database))
        # Get energy production for each number of changes
        energy_prod = [max_energy]

        for chosen_number_of_changes in table_number_of_changes:
            rs_results = ReorientationResults(location, chosen_number_of_changes)
            energy_prod.append(rs_results.annual_energy_produced)

        # Get production gain from energy production
        production_gain = []
        energy_reference = energy_prod[0]

        for power in energy_prod:
            production_gain.append((abs(power - energy_reference) / energy_reference) * 100)

        df_to_plot[location_name] = production_gain

    main_axis = df_to_plot.plot(kind='bar',
                                color=['#2899D5', '#472C7A', '#46BA50', '#CCE716'],
                                width=0.9)

    # Adding label value on each bar (and remove the '0%')
    for container in main_axis.containers:
        main_axis.bar_label(container,
                            labels=[f'{round(label_value, 1)}' if label_value != 0
                                    else '' for label_value in container.datavalues])

    main_axis.set_ylabel(r'Energy production gain (in \%)')

    main_axis.set_xticks(ticks=list(range(len(table_number_of_changes) + 1)),
                         labels=['Reference:\nNo reorientation'] + table_number_of_changes,
                         rotation='horizontal')

    main_axis.legend()

    fig_path = get_path(PAPER_FIGURE_FOLDER_PATH, 'reorientation_energy_gain_all_loc.pdf')

    plt.savefig(fig_path)

    logging.info(f'Figure saved in {fig_path}')
