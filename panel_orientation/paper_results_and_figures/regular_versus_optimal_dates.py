import itertools as it

import numpy as np
from more_itertools import pairwise

from common.consts import POWER_PRODUCED
from common.progress_counter import ProgressCounter
from common.utils import get_int_from_date_str
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from optimal_dates.orientation_file_reader import get_orientation_records
from optimal_tilt.tilt_and_power_finder import get_best_tilt_and_power_from_period_vector


def print_comparison_regular_optimal_cut(location_names: list, n_changes: int):
    """
    Print the comparison between reg cut and opti
    :param location_names: list of the LOCATIONS
    :param n_changes: number of reorientation
    """
    print(f'For N = {n_changes}:')

    for location_name in location_names:
        location = LocationStore().get_pv_lib_location(location_name)
        production_database = DailyEnergyProducedReader(location)

        def from_combinations_to_period(raw_combinations: tuple) -> np.array:
            """
            This returns the period array for each raw combination : [[0, d1], [d1, d2] ... [dn, 365]]
            It converts the raw_combination list in an array of date range for the future calculation
            :param raw_combinations: list which contains each period size [d1, d2 - d1, d3 - d2, ... , dn - dn-1]
            :return: period array
            """
            return np.array([[i, j] for i, j in pairwise(it.chain((0,), raw_combinations, (365,)))])

        if n_changes == 4:
            d_start = get_int_from_date_str('05/05')
            period_array = from_combinations_to_period(
                (d_start, d_start + 365 // 4, d_start + 365 // 2, d_start + (3 * 365) // 4))

        elif n_changes == 2:
            d_start = get_int_from_date_str('03/22')
            period_array = from_combinations_to_period(
                (d_start, d_start + 365 // 2))

        else:
            raise ValueError('N_changes must be 2 or 4!')

        bf_counter = ProgressCounter(1)

        res = get_best_tilt_and_power_from_period_vector(production_database,
                                                         period_array,
                                                         bf_counter)

        ener_opti = float(get_orientation_records(location=location, number_of_changes=n_changes).loc[POWER_PRODUCED])
        ener_reg = res[1]

        print(location_name)
        print(f'Energy produced regular cut: {ener_reg}')
        print(f'Energy produced optimal cut: {ener_opti}')
        print(f'Gain: {(ener_opti - ener_reg) / ener_reg * 100} %')
