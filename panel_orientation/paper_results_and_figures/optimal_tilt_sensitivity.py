import logging

import numpy as np
from pandas import DataFrame
import pylab
from matplotlib import pyplot as plt

from common.consts import TILT_ANGLE
from common.utils import get_path
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from optimal_tilt.tilt_and_power_finder import get_annual_best_tilt
from paper_results_and_figures.consts import PAPER_FIGURE_FOLDER_PATH

plot_parameters = {'font.size': 12,
                   'legend.title_fontsize': 24,
                   'legend.fontsize': 18,
                   'figure.figsize': (16, 9),
                   'figure.titlesize': 32,
                   'figure.labelsize': 18,
                   'axes.labelsize': 28,
                   'lines.linewidth': 2.5,
                   'axes.titlesize': 32,
                   'image.cmap': 'plasma',
                   'figure.dpi': 300}


def get_sensitivity_db(loc_name: str, threshold: float) -> tuple:
    """
    Create a dataframe dedicated to tilt sensitivity using the production database
    :param loc_name: location name
    :param threshold: find the tilt range for a losses associated to the threshold
    """
    location = LocationStore().get_pv_lib_location(location_name=loc_name)
    production_database = DailyEnergyProducedReader(location=location)

    sensitivity_tilt_calculs = DataFrame({TILT_ANGLE: production_database.tilt_table})
    sensitivity_tilt_calculs['Energy_produced'] = sensitivity_tilt_calculs.apply(
        lambda x: production_database.get_annual_energy_produced_from_tilt(tilt_angle=x[TILT_ANGLE]), axis=1)

    best_tilt, max_energy = tuple(get_annual_best_tilt(production_database))

    sensitivity_tilt_calculs['Centered_Tilt'] = sensitivity_tilt_calculs.apply(
        lambda x: x[TILT_ANGLE] - best_tilt,
        axis=1)
    sensitivity_tilt_calculs['Energy_Losses'] = sensitivity_tilt_calculs.apply(
        lambda x: 100 * (x['Energy_produced'] - max_energy) / max_energy,
        axis=1)

    sensitivity_tilt_calculs['Acceptable_tilt'] = sensitivity_tilt_calculs.apply(
        lambda x: x[TILT_ANGLE] if x['Energy_produced'] > threshold * max_energy else None,
        axis=1)

    tilt_min = np.nanmin(sensitivity_tilt_calculs['Acceptable_tilt'])
    tilt_max = np.nanmax(sensitivity_tilt_calculs['Acceptable_tilt'])

    logging.info(f'In {loc_name}, to keep the annual energy produced over {threshold * 100}% \n'
                 f'of the maximum energy obtained with the optimal tilt: {best_tilt}°\n'
                 f'The tilt angle must be set between: {tilt_min}° and {tilt_max}°'
                 )
    return production_database, sensitivity_tilt_calculs


def plot_annual_tilt_sensitivity(location_table: list, threshold: float):
    """
    Annual sensitivity plots
    :param location_table: list of the location names
    :param threshold: threshold applied to find the tilt range of acceptable tilt in terms of energy production
    """
    best_tilt = None
    max_energy = None

    pylab.rcParams.update(plot_parameters)

    _, axis = plt.subplots(ncols=2)
    raw_axis, centered_axis = axis[0], axis[1]

    for loc_name in location_table:
        production_database, sensitivity_tilt_calculs = get_sensitivity_db(loc_name, threshold)
        raw_axis.scatter(best_tilt, max_energy, marker='X', c='black')
        raw_axis.plot(production_database.tilt_table,
                      sensitivity_tilt_calculs['Energy_produced'],
                      label=loc_name)

        centered_axis.plot(sensitivity_tilt_calculs['Centered_Tilt'][
                               (sensitivity_tilt_calculs.Centered_Tilt <= 20) & (
                                       sensitivity_tilt_calculs.Centered_Tilt >= -20)],
                           sensitivity_tilt_calculs['Energy_Losses'][
                               (sensitivity_tilt_calculs.Centered_Tilt <= 20) & (
                                       sensitivity_tilt_calculs.Centered_Tilt >= -20)],
                           label=loc_name)

        raw_axis.legend()

    centered_axis.grid(True, which='both')
    raw_axis.grid(True, which='both')
    centered_axis.axhline(y=0, color='k')
    centered_axis.axvline(x=0, color='k')
    raw_axis.set_xlabel(r'$\theta$ (°)')
    raw_axis.set_ylabel(r'$E_{\theta}$ (Wh)')
    raw_axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    centered_axis.set_xlabel(r'$\theta_{centered}$ (°)')
    centered_axis.set_ylabel(r'$E_{losses}$ ($\%$)')

    fig_path = get_path(PAPER_FIGURE_FOLDER_PATH, 'optimal_tilt_sensitivity.png')
    plt.savefig(fig_path)
