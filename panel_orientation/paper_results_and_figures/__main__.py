import logging

from matplotlib import pylab

from common.utils import get_int_from_date_str
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from optimal_dates.reorientation_results import ReorientationResults
from optimal_tilt.short_period_results import ShortPeriodResults
from paper_results_and_figures.consts import LOCATIONS, TABLE_NUM_OF_CHANGES
from paper_results_and_figures.energy_gain_plotter import plot_gain_study_multiple_locations
from paper_results_and_figures.optimal_tilt_sensitivity import plot_annual_tilt_sensitivity
from paper_results_and_figures.plotter_daily_energy import plot_daily_energy
from paper_results_and_figures.regular_versus_optimal_dates \
    import print_comparison_regular_optimal_cut
from plotter.reorientation_plotter import plot_multiple_tilt

# Mode 0:
# Calculation of all the simulation files used in the paper

# Mode 1:
# Plot the energy gain for all locations

# Mode 2:
# Save the figure of N reorientation(s) for the selected location

# Mode 3:
# Compare optimal and regular tilts for all locations

# Mode 4:
# Short period plot and export capture

# Mode 5:
# Generate daily energy produced figure for all locations

# Mode 6:
# Generate tilt sensitivity figure for all locations

MODE = 5


def main():
    logging.basicConfig(level=logging.INFO)

    pylab.rcParams.update({'text.usetex': True,
                           "font.family": "serif",
                           "font.serif": ["Times",
                                          "Palatino",
                                          "New Century Schoolbook",
                                          "Bookman",
                                          "Computer Modern Roman"]})

    if MODE == 0:
        for loc_name in LOCATIONS:
            for n_changes in TABLE_NUM_OF_CHANGES:
                current_loc = LocationStore().get_pv_lib_location(loc_name)
                ReorientationResults(current_loc, n_changes)

    elif MODE == 1:
        plot_gain_study_multiple_locations(LOCATIONS, TABLE_NUM_OF_CHANGES)

    elif MODE == 2:
        number_of_changes = 4
        location = LocationStore().get_pv_lib_location('Reykjavik')
        plot_multiple_tilt(ReorientationResults(location, number_of_changes), show_instruction=True)

    elif MODE == 3:
        number_of_changes = 4
        print_comparison_regular_optimal_cut(LOCATIONS, number_of_changes)

    elif MODE == 4:
        location_name = 'Brasilia'
        location = LocationStore().get_pv_lib_location(location_name)
        production_database = DailyEnergyProducedReader(location=location)
        start = get_int_from_date_str('06/01')
        end = get_int_from_date_str('07/01')

        sp_results = ShortPeriodResults(location=location,
                                        start=start,
                                        end=end)
        print(str(sp_results))

        annual_tilt_ener = production_database.get_energy_from_tilt_and_period(start=start, end=end, tilt_angle=21)
        opti_tilt_ener = sp_results.total_energy

        latitude_tilt_ener = production_database.get_energy_from_tilt_and_period(start=start, end=end, tilt_angle=16)
        print(f'Energy loss lat: {round(100 * (opti_tilt_ener - latitude_tilt_ener) / opti_tilt_ener, 2)}%')
        print(f'Energy loss annual tilt: {round(100 * (opti_tilt_ener - annual_tilt_ener) / opti_tilt_ener, 2)}%')

    elif MODE == 5:
        plot_daily_energy(LOCATIONS)

    elif MODE == 6:
        threshold = 0.99
        plot_annual_tilt_sensitivity(LOCATIONS, threshold)


if __name__ == "__main__":
    main()
