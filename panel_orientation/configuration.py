from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS

# Brute force method parameters: impacts the simulation time and the accuracy of the results
PERIOD_RESOLUTION = 52
TILT_RESOLUTION = 1

# PV_Lib main parameters, see documentation here: https://pvlib-python.readthedocs.io/en/stable/
MODULE_PARAMETERS = {'pdc0': 240, 'gamma_pdc': -0.004}
INVERTER_PARAMETERS = {'pdc0': 240}
TEMPERATURE_MODEL = TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_glass']
AOI_MODEL = 'physical'
SPECTRAL_MODEL = 'no_loss'
AZIMUTH_ANGLE = None

# Forecast model used to predict DNI, DHI, GHI etc.
# Available options are CLEAR_SKY and PVGIS
PVGIS = 'pvgis'
CLEAR_SKY = 'clear_sky'
FORECAST_MODEL = PVGIS
