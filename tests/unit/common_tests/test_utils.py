import unittest

from common.utils import get_tilt_table, get_int_from_date_str, get_number_of_days_in_year, \
    get_str_date_from_int, get_scale_factor, get_month_names


class TestUtils(unittest.TestCase):
    """
    Unit tests for Utils
    """

    def test_get_tilt_range(self):
        # Test with latitude in the northern hemisphere
        latitude = 45.0
        tilt_resolution = 5
        expected_tilt_range = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85]
        self.assertEqual(get_tilt_table(latitude, tilt_resolution), expected_tilt_range)

        # Test with latitude in the southern hemisphere
        latitude = -30.0
        tilt_resolution = 10
        expected_tilt_range = [-15, -5, 5, 15, 25, 35, 45, 55, 65, 75]
        self.assertEqual(get_tilt_table(latitude, tilt_resolution), expected_tilt_range)

    def test_get_int_from_date_str(self):
        self.assertEqual(get_int_from_date_str("01/01"), 0)
        self.assertEqual(get_int_from_date_str("02/01"), 31)
        self.assertEqual(get_int_from_date_str("03/01"), 59)
        self.assertEqual(get_int_from_date_str("12/31"), 364)

    def test_get_number_of_days_in_year(self):
        self.assertEqual(get_number_of_days_in_year(2022), 365)
        self.assertEqual(get_number_of_days_in_year(2023), 365)
        self.assertEqual(get_number_of_days_in_year(2024), 366)

    def test_get_str_date_from_int(self):
        self.assertEqual(get_str_date_from_int(0), "01 Jan")
        self.assertEqual(get_str_date_from_int(59), "01 Mar")
        self.assertEqual(get_str_date_from_int(364), "31 Dec")

    def tests_get_scale_factor(self):
        self.assertEqual(get_scale_factor(2020, 52), 7)

    def test_get_month_names(self):
        expected_month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        self.assertEqual(get_month_names(), expected_month_names)
