import unittest

import numpy as np
from pandas import DataFrame

from common.consts import COMBINATION_VECTOR, PERIOD_ARRAY
from optimal_dates.combinations_creator import get_combinations


class TestGetPartition(unittest.TestCase):
    """
    Unit tests for GetPartition
    """

    def test_get_partition(self):
        # given
        period_resulution = 4
        changes_count = 3

        # when
        output = get_combinations(changes_count, period_resolution=period_resulution)

        # then
        expected_output = DataFrame({
            COMBINATION_VECTOR: [(1, 2, 3), (1, 2, 4), (1, 3, 4), (2, 3, 4)],
            PERIOD_ARRAY: [
                np.array([[0, 91], [91, 182], [182, 273], [273, 364]]),
                np.array([[0, 91], [91, 182], [182, 364], [364, 364]]),
                np.array([[0, 91], [91, 273], [273, 364], [364, 364]]),
                np.array([[0, 182], [182, 273], [273, 364], [364, 364]])
            ]
        })
        self.assertTrue(expected_output.equals(output))
