import unittest

from pandas import DataFrame

from common.consts import POWER_PRODUCED, COMBINATION_VECTOR, TEST_RESULT_FOLDER_PATH, \
    TEST_LOCATION_FOLDER_PATH
from location_and_panel.location_store import LocationStore
from optimal_dates.orientation_file_reader import get_orientation_records


class TestGetOrientationRecords(unittest.TestCase):
    """
    Unit tests for GetOrientationRecords
    """

    def test_get_orientation_records(self):
        # given
        location = LocationStore(TEST_LOCATION_FOLDER_PATH).get_pv_lib_location('Sherbrooke')
        number_of_changes = 2

        # when
        result = get_orientation_records(location=location,
                                         number_of_changes=number_of_changes,
                                         result_folder_path=TEST_RESULT_FOLDER_PATH)

        combination_vector = '(13, 35)'
        power_produced = 362047
        data_true = DataFrame({COMBINATION_VECTOR: [combination_vector],
                               POWER_PRODUCED: [power_produced]}).transpose()

        self.assertListEqual(list(result.loc[COMBINATION_VECTOR]), list(data_true.loc[COMBINATION_VECTOR]))
        self.assertAlmostEqual(float(result.loc[POWER_PRODUCED]), float(data_true.loc[POWER_PRODUCED]),
                               delta=1)
