import unittest

from pandas import DataFrame

from common.consts import TEST_PROD_FOLDER_PATH, TEST_LOCATION_FOLDER_PATH
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore


class TestDailyEnergyProductionReader(unittest.TestCase):
    """
    Unit tests for DailyEnergyProductionReader
    """
    def setUp(self):
        self.location = LocationStore(TEST_LOCATION_FOLDER_PATH).get_pv_lib_location(location_name='Sherbrooke')
        self.reader = DailyEnergyProducedReader(self.location, prod_folder_path=TEST_PROD_FOLDER_PATH)

    def test_get_production_records(self):
        """
        Test if the dataframe is stored
        """
        production_df = self.reader.daily_energy_prod_df
        self.assertIsInstance(production_df, DataFrame)
        self.assertGreater(len(production_df), 0)

    def test_get_energy_from_tilt_and_period_without_overlapping(self):
        """
        Test if the energy computation for a period and tilt value is valid
        Start is before End
        """
        energy_produced_true = 15298
        energy_produced = self.reader.get_energy_from_tilt_and_period(20, 0, 30)

        self.assertAlmostEqual(energy_produced, energy_produced_true, delta=1)

    def test_get_energy_from_tilt_and_period_with_overlapping(self):
        """
        Test if the energy computation for a period and tilt value is valid
        Start is after End (overlapping)
        """
        energy_produced_true = 10759
        energy_produced = self.reader.get_energy_from_tilt_and_period(20, 350, 10)

        self.assertAlmostEqual(energy_produced, energy_produced_true, delta=1)

    def test_get_annual_energy_produced_from_tilt(self):
        energy_produced_true = 341352
        energy_produced = self.reader.get_annual_energy_produced_from_tilt(20)
        self.assertAlmostEqual(energy_produced, energy_produced_true, delta=1)
