import unittest
from unittest import mock


from common.consts import TEST_LOCATION_FOLDER_PATH
from location_and_panel.location_store import LocationStore


class TestLocationStore(unittest.TestCase):
    """
    Unit tests for LocationStore
    """

    def setUp(self):
        # given
        self.location_store = LocationStore(
            location_folder_path=TEST_LOCATION_FOLDER_PATH)

    def test_is_location_in_store(self):
        # Test with an existing location
        self.assertTrue(self.location_store.is_location_in_store('Sherbrooke'))
        # Test with a non-existing location
        self.assertFalse(self.location_store.is_location_in_store('Atlantis'))

    @mock.patch('pandas.DataFrame.to_csv')
    def test_add_a_location(self, mock_to_csv):
        # given
        location_name = 'New York'
        lat = 40.7128
        lon = -74.0060

        mock_to_csv.assert_not_called()
        # Check that the location doesn't exist in the database before adding it
        self.assertFalse(self.location_store.is_location_in_store(location_name))

        # when
        # Add the new location to the database
        self.location_store.add_a_location(location_name, lat, lon)

        # then
        # Check that the location now exists in the database
        self.assertTrue(self.location_store.is_location_in_store(location_name))

    def test_get_pv_lib_location(self):
        # Test with an existing location
        # given
        location_name = 'Sherbrooke'

        # when
        pvlib_location = self.location_store.get_pv_lib_location(location_name)

        # then
        self.assertEqual(pvlib_location.name, location_name)
        self.assertEqual(pvlib_location.latitude, 45.4048)
        self.assertEqual(pvlib_location.longitude, -71.8928)

        # Test with a non existing location
        # given
        location_name = 'Atlantis'
        # then
        with self.assertRaises(ValueError):
            self.location_store.get_pv_lib_location(location_name)

    def test_get_pv_lib_location_info(self):
        # given
        location_name = 'Sherbrooke'

        # when
        output = self.location_store.get_pv_lib_location_info(location_name)

        # then
        expected_output = f'name : {location_name}\nlat : 45.4048\nlon : -71.8928\n'
        self.assertEqual(output, expected_output)

        # Test with a non existing location
        # given
        location_name = 'Atlantis'
        # then
        with self.assertRaises(ValueError):
            self.location_store.get_pv_lib_location_info(location_name)
