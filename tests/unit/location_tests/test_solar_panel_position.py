import unittest

from common.consts import TEST_LOCATION_FOLDER_PATH
from location_and_panel.location_store import LocationStore
from location_and_panel.solar_panel_position import SolarPanelPosition


class TestSolarPanelPosition(unittest.TestCase):
    """
    Unit tests for SolarPanelPosition
    """

    def setUp(self) -> None:
        # given
        self.test_location = LocationStore(TEST_LOCATION_FOLDER_PATH).get_pv_lib_location(location_name='Sherbrooke')
        self.panel = SolarPanelPosition(self.test_location, 30)

    def test_init(self):
        # when
        azimuth = self.panel.panel_azimuth_angle

        # then
        expected_azimuth = 180
        self.assertEqual(azimuth, expected_azimuth)

    def test_increment_tilt_angle(self):
        # given
        increment = 10

        # when
        self.panel.increment_tilt_angle(increment)
        tilt_angle = self.panel.panel_tilt_angle

        # then
        expected_tilt_angle = 40
        self.assertEqual(tilt_angle, expected_tilt_angle)
