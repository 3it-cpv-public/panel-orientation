import unittest

from common.consts import TEST_PROD_FOLDER_PATH, TEST_LOCATION_FOLDER_PATH
from common.progress_counter import ProgressCounter
from daily_energy_production.daily_energy_produced_reader import DailyEnergyProducedReader
from location_and_panel.location_store import LocationStore
from optimal_tilt.tilt_and_power_finder import get_best_tilt_and_power_from_period, \
    get_best_tilt_and_power_from_period_vector, get_annual_best_tilt


class TestGetTiltAndPower(unittest.TestCase):
    """
    Unit tests for GetTiltAndPower
    """

    def setUp(self):
        # given
        location = LocationStore(TEST_LOCATION_FOLDER_PATH).get_pv_lib_location('Sherbrooke')
        self.production_database = DailyEnergyProducedReader(location, prod_folder_path=TEST_PROD_FOLDER_PATH)

    def test_get_tilt_and_power_from_period(self):
        # given
        start = 0
        end = 90

        # when
        output = get_best_tilt_and_power_from_period(self.production_database, start, end)

        # then
        expected_output = (54, 76069.9547239)
        self.assertEqual(expected_output, output)

    def test_get_best_tilt_and_power_from_period_vector(self):
        # given
        period_array = [[0, 100], [100, 200], [200, 365]]

        # when
        bf_counter = ProgressCounter(len(period_array), print_progress=False)
        output = get_best_tilt_and_power_from_period_vector(self.production_database, period_array, bf_counter)

        # then
        expected_output = [[(46, 223647.56265581428), (15, 136048.3024825968)], 359695.86513841106]
        self.assertEqual(expected_output, output)

    def test_get_annual_best_tilt(self):
        # when
        output = get_annual_best_tilt(self.production_database)

        # then
        expected_output = (36, 350755.1814301632)
        self.assertEqual(expected_output, output)
