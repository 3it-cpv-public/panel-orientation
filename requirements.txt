# specific for development/CI configuration
setuptools==53.0.0
check-manifest==0.46
coverage==6.3.2
coverage-threshold==0.4.4
pylint==2.15.10

# common libraries
plotly==5.12.0
pandas==1.5.3
matplotlib==3.6.3
numpy==1.24.1
scipy==1.10.0
pytz==2022.7.1

# specific for the panel_orientation module
suncalc==0.1.2
pvlib==0.9.5
more-itertools==9.0.0
